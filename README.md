# video-encoding-test

You need FFmpeg installed to run these tests.

## Test file

Download https://download.slideslive.com/video-encoding-test/input.mp4 to this repository's directory.

## Parallel encoding without Docker

Tests only h264. Run in this repository's directory. Statistics about performance will be saved to `local/stats.txt`.

```
./parallel_test.rb --name local --input-file input.mp4 --presets ultrafast,veryfast --parallel 1,2,3,4,5,6,7,8,9,10
./parallel_test.rb --name local --input-file input.mp4 --presets fast,medium --parallel 1,2,3,4,5,6,7,8
./parallel_test.rb --name local --input-file input.mp4 --presets slower,veryslow --parallel 1,2,3,4,5,6
```

## Parallel encoding with NVIDIA in Docker

Tests both h264 and h264_nvenv encoders. Run in this repository's directory. Statistics about performance will be saved to `nv/stats.txt`.

Use `NVIDIA_VISIBLE_DEVICES=<gpu-uuid>` environment variable for Docker container to select which GPU to use for testing. Use `nvidia-smi -L` to list available GPUs.

```
docker run --runtime=nvidia -e NVIDIA_VISIBLE_DEVICES=GPU-9abd32f0-305d-206d-08a4-2c29e75e7cd1 -e NVIDIA_DRIVER_CAPABILITIES=compute,utility,video -ti -v $(pwd):/d video_platform_test:latest /bin/bash -lc "cd /d && ./parallel_test.rb --name nv --input-file input.mp4 --presets ultrafast,veryfast --parallel 1,2,3,4,5,6,7,8,9,10 --nv-presets fast,medium,slow --nv-parallel 1,2"
```
