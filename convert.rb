#!/usr/bin/env ruby

# YouTube recommendations
# Audio: AAC or MP3, 44.1 KHz, 128 Kbps
# 240p: 426x240, 300 - 700 Kbps, 400 Kbps
# 360p: 640x360, 400 - 1000 Kbps, 750 Kbps
# 480p: 854x480, 500 - 2000 Kbps, 1000 Kbps
# 720p: 1280x720, 1500 - 4000 Kbps, 2500 Kbps
# 720p @ 60 fps: 1280x720, 2250 - 6000 Kbps
# 1080p: 1920x1080, 3000 - 6000 Kbps, 4500 Kbps
# 1080p @ 60 fps: 1920x1080, 4500 - 9000 Kbps
# 1440p @ 30 fps: 2560x1440, 6000 - 13000 Kbps
# 1440p @ 60 fps: 2560x1440, 9000 - 18000 Kbps
# 4k / 2160p @ 30 fps: 3840x2160, 13000 - 34000 Kbps
# 4x / 2160p @ 60 fps: 3840x2160, 20000 - 51000 Kbps


input_path = ARGV[0]
output_path = ARGV[1]
preset = ARGV[2] || 'medium'
use_nvenc = ARGV[3] == 'nvenc'
use_nvdec = ARGV[4] == 'nvdec'

gop_size = 100
fps = 25
hls_time = 4
crf = 21

qualities = [
  {
    size: '1280x720',
    bitrate: 2500,
    maxrate: nil,
    bufsize: nil
  },
  {
    size: '416x234',
    bitrate: 145,
    maxrate: nil,
    bufsize: nil
  },
  {
    size: '640x360',
    bitrate: 365,
    maxrate: nil,
    bufsize: nil
  },
  {
    size: '768x432',
    bitrate: 730,
    maxrate: nil,
    bufsize: nil
  },
  {
    size: '1920x1080',
    bitrate: 4500,
    maxrate: nil,
    bufsize: nil
  }
]

ffmpeg = []
ffmpeg << 'ffmpeg -y -nostdin'

if use_nvenc && use_nvdec
  ffmpeg << '-hwaccel cuda -hwaccel_output_format cuda'
end

ffmpeg << "-i \"#{input_path}\""
ffmpeg << "-preset #{preset} -keyint_min #{gop_size} -g #{gop_size} -sc_threshold 0"
ffmpeg << "-r #{fps} -c:a aac -b:a 128k -ac 1 -ar 44100"

if use_nvenc
  ffmpeg << '-c:v h264_nvenc'
else
  ffmpeg << '-c:v libx264 -pix_fmt yuv420p'
end

qualities.each_with_index do |q, i|
  maxrate = (q[:maxrate] || (q[:bitrate] * 1.1)).to_i
  bufsize = (q[:bufsize] || (q[:bitrate] * 1.75)).to_i
  
  if use_nvenc && use_nvdec
    ffmpeg << "-map v:0 -filter:v:#{i} scale_npp=#{q[:size].sub('x', ':')} -b:v:#{i} #{q[:bitrate]}k -maxrate:#{i} #{maxrate}k -bufsize:#{i} #{bufsize}k"
  else
    ffmpeg << "-map v:0 -s:#{i} #{q[:size]} -b:v:#{i} #{q[:bitrate]}k -maxrate:#{i} #{maxrate}k -bufsize:#{i} #{bufsize}k"
  end
end

ffmpeg << '-map 0:a'
ffmpeg << '-init_seg_name init\$RepresentationID\$.\$ext\$ -media_seg_name chunk\$RepresentationID\$-\$Number%05d\$.\$ext\$'
ffmpeg << '-use_template 1 -use_timeline 1 -dash_segment_type mp4'
ffmpeg << "-seg_duration #{hls_time} -adaptation_sets \"id=0,streams=v id=1,streams=a\""
ffmpeg << '-hls_playlist 1' # -hls_master_name "master.m3u8"'
ffmpeg << "-f dash \"#{output_path}\""

ffmpeg_cmd = ffmpeg.join(' ')

STDERR.puts(ffmpeg_cmd)

`#{ffmpeg_cmd}`
