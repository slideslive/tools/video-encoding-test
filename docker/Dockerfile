FROM nvidia/cuda:11.0-devel-ubuntu20.04

ENV TZ UTC

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV CUDA_VISIBLE_DEVICES 0

SHELL ["/bin/bash", "-l", "-c"]

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone \
    && echo en_US.UTF-8 UTF-8 > /etc/locale.gen \
    && apt-get update -qq \
    && apt-get install -y --no-install-recommends locales \
    && apt-get install -y --no-install-recommends \
        git unzip wget \
        libssl-dev libreadline-dev \
        nasm yasm libx264-dev libx265-dev libnuma-dev libmp3lame-dev libass-dev \
    && apt-get clean autoclean \
    && apt-get autoremove -y \
    && rm -rf \
        /var/lib/cache/* \
        /var/lib/log/* \
    && echo "dash dash/sh boolean false" | debconf-set-selections \
    && dpkg-reconfigure -f noninteractive dash

WORKDIR /w

RUN git clone https://git.videolan.org/git/ffmpeg/nv-codec-headers.git \
    && cd nv-codec-headers \
    && make -j$(nproc) \
    && make install PREFIX=/usr/local

RUN wget -q http://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2 -O ffmpeg-snapshot.tar.bz2 \
    && tar xjf ffmpeg-snapshot.tar.bz2 \
    && cd ffmpeg \
    && ./configure \
      --prefix=/usr/local \
      --extra-libs="-lpthread -lm" \
      --extra-cflags=-I/usr/local/cuda/include \
      --extra-cflags=-I/usr/local/include \
      --extra-ldflags=-L/usr/local/cuda/lib64 \
      --enable-shared \
      --enable-gpl \
      --disable-libaom \
      --enable-libmp3lame \
      --enable-libx264 \
      --enable-nonfree \
      --enable-avresample \
      --enable-cuda \
      --enable-cuvid \
      --enable-libnpp || cat ffbuild/config.log \
    && make -j$(nproc) \
    && make install

ENV RUBY_VERSION 2.6.6
ENV RBENV_ROOT /usr/local/rbenv

RUN git clone --depth 1 https://github.com/sstephenson/rbenv.git $RBENV_ROOT \
    && git clone --depth 1 https://github.com/sstephenson/ruby-build.git $RBENV_ROOT/plugins/ruby-build \
    && echo '# rbenv setup' > /etc/profile.d/rbenv.sh \
    && echo "export RBENV_ROOT=$RBENV_ROOT" >> /etc/profile.d/rbenv.sh \
    && echo 'export PATH="$RBENV_ROOT/bin:$PATH"' >> /etc/profile.d/rbenv.sh \
    && echo 'eval "$(rbenv init -)"' >> /etc/profile.d/rbenv.sh \
    && chmod +x /etc/profile.d/rbenv.sh \
    && . /etc/profile.d/rbenv.sh \
    && CONFIGURE_OPTS=--disable-install-doc rbenv install $RUBY_VERSION \
    && rbenv global $RUBY_VERSION \
    && echo 'gem: --no-rdoc --no-ri' >> $HOME/.gemrc \
    && which ruby \
    && ruby -v \
    && gem install bundler
