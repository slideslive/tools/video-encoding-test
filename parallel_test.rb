#!/usr/bin/env ruby

require 'fileutils'
require 'optparse'

NAME_RX = /^[a-z0-9_-]+$/i

def parse_options
  options = {
    name: nil,
    input_files: [],
    nv_presets: %w[fast medium slow llhp llhq],
    nv_parallel: nil,
    presets: %w[ultrafast veryfast fast medium slower veryslow],
    parallel: nil
  }

  OptionParser.new do |parser|
    parser.on('', '--name NAME', '') do |s|
      options[:name] = s.strip
    end

    parser.on('', '--input-file FILE', '') do |s|
      options[:input_files] << s.strip
    end

    parser.on('', '--nv-presets NV_PRESETS', '') do |s|
      options[:nv_presets] = s.strip.split(',').map(&:strip).map(&:downcase)
    end

    parser.on('', '--nv-parallel NV_PARALLEL', '') do |s|
      options[:nv_parallel] = s.strip.split(',').map(&:strip).map(&:to_i)
    end

    parser.on('', '--presets PRESETS', '') do |s|
      options[:presets] = s.strip.split(',').map(&:strip).map(&:downcase)
    end

    parser.on('', '--parallel PARALLEL', '') do |s|
      options[:parallel] = s.strip.split(',').map(&:strip).map(&:to_i)
    end
  end.parse!

  %i[name input_files].each do |key|
    raise OptionParser::MissingArgument, name if options[key].nil? || options[key].empty?
  end

  raise ArgumentError, "--name must match #{NAME_RX.inspect}" unless options[:name].match(NAME_RX)

  options
end

def measure
  started_at = Process.clock_gettime(Process::CLOCK_MONOTONIC)
  yield
  ended_at = Process.clock_gettime(Process::CLOCK_MONOTONIC)

  (ended_at - started_at).to_i
end

def convert(test_name, input_file, use_nvenc, use_nvdec, preset, count)
  name = File.basename(input_file, '.*').gsub(' ', '_')
  name_subdir = ["parallel", name, preset]
  name_subdir << 'nvenc' if use_nvenc
  name_subdir << 'nvdec' if use_nvdec
  name_subdir << test_name
  name_subdir = name_subdir.join('_')

  output_paths = []
  count.times do |i|
    output_paths << File.join(test_name, name_subdir + "_#{count}_#{i}", "master.mpd")
  end

  ms = measure do
    pids = []

    output_paths.each_with_index do |output_path, i|
      pids << fork do
        output_dir = File.dirname(output_path)
        FileUtils.mkdir_p(output_dir)

        `./convert.rb "#{input_file}" "#{output_path}" #{preset} #{use_nvenc ? 'nvenc' : ''} #{use_nvdec ? 'nvdec' : ''} 2> #{output_dir}.log`
      end
    end

    while pids.any?
      pid = Process.waitpid
      pids.delete(pid)
    end
  end

  File.open(File.join(test_name, 'stats.txt'), 'a') do |f|
    f << [name_subdir, count, ms.to_s].join(',')
    f << "\n"
  end
ensure
  output_paths.each do |output_path|
    FileUtils.rm_rf(File.dirname(output_path))
  end
end

options = parse_options

pp options

FileUtils.mkdir_p(options[:name])

options[:input_files].each do |input_file|
  options[:nv_presets]&.each do |preset|
    options[:nv_parallel]&.each do |count|
      pp ['nv', preset, count]

      convert(options[:name], input_file, true, true, preset, count)
    end
  end

  options[:presets]&.each do |preset|
    options[:parallel]&.each do |count|
      pp [preset, count]

      convert(options[:name], input_file, false, false, preset, count)
    end
  end
end
